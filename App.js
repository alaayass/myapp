import React from 'react';
import Main from './components/MainComponent';
import { db } from './components/config';
import { Provider } from 'react-redux';
import { ConfigureStore } from './redux/configureStore';
import * as firebase from 'firebase';
import Login from './components/login';



/*var config = {
  apiKey: "AIzaSyCgR0Ff7BfeXvJlKxmkgzU321v_lbgV2C4",
  authDomain: "omraapp-fd152.firebaseapp.com",
  databaseURL: "https://omraapp-fd152.firebaseio.com",
  projectId: "omraapp-fd152",
  storageBucket: "omraapp-fd152.appspot.com",
  messagingSenderId: "150893156453",
  appId: "1:150893156453:web:b0a49a028c41392e"
 
};

firebase.initializeApp(config);*/
/*setTimeout(() => {
            firebase.database().ref('reservation/003').set(
                {
                    name: 'الأسواف',
                    description: 'حملة راقية بمبلغ 350 ريال للشخص و تتضمن الحملة تكاليف الطعام و الشراب',
                    city: 'جدة'
                }
            ).then(() => {
                console.log('INSERTED !');
            }).catch((error) => {
                console.log(error);
            });
        }, 5000);*/


firebase.auth().onAuthStateChanged((user) => {
  if (user != null) {
    console.log("We are authenticated now!");
  }
  });


const store = ConfigureStore();

export default function App() {
  return (
    <Provider store={store}>
    <Main />
    </Provider>
  );
}