export const OFFERS =
    [
        {
        id: 0,
        name:'دار الطيبون للعمرة و الحج',
        image: 'images/Picture3.png',
        category: 'Bus',
        price:'160',
        featured: true,
        description:'عرض خاص . حلة عمرة من القصيم للرياض بالباص بكلفة 160 ريال للشخص'                    
        },
        {
        id: 1,
        name:'حملة الخير',
        image: 'images/Picture3.png',
        category: 'Bus',
        price:'200',
        featured: false,
        description: 'حملة الخير تقدم رحلات بالباص إلى مكة و المدينة بكلفة 200 ريال للشخص'
        },
        {
        id: 2,
        name:'الأسواف',
        image: 'images/Picture3.png',
        category: 'Bus',
        price:'300',
        featured: false,
        description: 'حملة راقية بمبلغ 300 ريال للشخص و تتضمن الحملة تكاليف الطعام و الشراب'
        },
        {
        id: 3,
        name:'حملة القصيم',
        image: 'images/Picture3.png',
        category: 'Bus',
        price:'120',
        featured: false,
        description:'حملة القصيم تنطلق من بريدة إلى مكة بكلفة 120 ريال للشخص'
        }
    ];