export const COMMENTS = 
[
    {
        id: 0,
        offerId: 0,
        rating: 5,
        comment: "حملة رائعة و عرض ممتاز",
        author: "عبد الله العنزي",
        date: "2012-10-16T17:57:28.556094Z"
    },
    {
        id: 1,
        offerId: 0,
        rating: 4,
        comment: "احجز لا توقف!",
        author: "محمود السعد",
        date: "2014-09-05T17:57:28.556094Z"
    },
    {
        id: 2,
        offerId: 0,
        rating: 3,
        comment: "الخدمة سيئة",
        author: "مسعود المطيري",
        date: "2015-02-13T17:57:28.556094Z"
    },
    {
        id: 3,
        offerId: 0,
        rating: 4,
        comment: "حملة جيدة و السعر مناسب",
        author: "وضحة العتيبي",
        date: "2013-12-02T17:57:28.556094Z"
    },
    {
        id: 4,
        offerId: 0,
        rating: 2,
        comment: "تأخير في الحجز",
        author: "احمد1980",
        date: "2011-12-02T17:57:28.556094Z"
    },
    {
        id: 5,
        offerId: 1,
        rating: 5,
        comment: "جزاكم الله خير",
        author: "آلاء الياسين",
        date: "2012-10-16T17:57:28.556094Z"
    },
    {
        id: 6,
        offerId: 1,
        rating: 4,
        comment: "لرحلة جيدة جدا",
        author: "سالم السلمي",
        date: "2018-09-05T17:57:28.556094Z"
    },
    {
        id: 7,
        offerId: 1,
        rating: 3,
        comment: "ممتازة",
        author: "محمد الشقيري",
        date: "2018-02-13T17:57:28.556094Z"
    },
    {
        id: 8,
        offerId: 1,
        rating: 4,
        comment: "احجزوا مرة ممتازة!",
        author: "هدى الحربي",
        date: "2017-12-02T17:57:28.556094Z"
    }
    
    
]