import * as ActionTypes from './ActionTypes';
import { baseUrl } from '../shared/baseUrl';

export const fetchComments = () => (dispatch) => {
    return fetch(baseUrl + 'comments')
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
      })
    .then(response => response.json())
    .then(comments => dispatch(addComments(comments)))
    .catch(error => dispatch(commentsFailed(error.message)));
};

export const commentsFailed = (errmess) => ({
    type: ActionTypes.COMMENTS_FAILED,
    payload: errmess
});

export const addComments = (comments) => ({
    type: ActionTypes.ADD_COMMENTS,
    payload: comments
});

export const fetchOffers = () => (dispatch) => {

    dispatch(offersLoading());

    return fetch(baseUrl + 'offers')
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
      })
    .then(response => response.json())
    .then(offers => dispatch(addOffers(offers)))
    .catch(error => dispatch(offersFailed(error.message)));
};

export const offersLoading = () => ({
    type: ActionTypes.OFFERS_LOADING
});

export const offersFailed = (errmess) => ({
    type: ActionTypes.OFFERS_FAILED,
    payload: errmess
});

export const addOffers = (offers) => ({
    type: ActionTypes.ADD_OFFERS,
    payload: offers
});

export const postFavorite = (offerId)  => (dispatch) => {

    setTimeout(() => {
        dispatch(addFavorite(offerId));
    }, 2000);
};


export const addFavorite = (offerId) => ({
    type: ActionTypes.ADD_FAVORITE,
    payload: offerId
});
