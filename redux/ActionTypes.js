export const OFFERS_LOADING = 'OFFERS_LOADING';
export const ADD_OFFERS = 'ADD_OFFERS';
export const OFFERS_FAILED = 'OFFERS_FAILED';
export const ADD_COMMENTS = 'ADD_COMMENTS';
export const COMMENTS_FAILED = 'COMMENTS_FAILED';
export const POST_FAVORITE = 'POST_FAVORITE';
export const ADD_FAVORITE = 'ADD_FAVORITE';