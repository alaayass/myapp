import React, { Component } from 'react';

import { View, Text, Image, Style, ImageBackground, Button, TextInput } from 'react-native';


class Contact extends Component {

     constructor(props) {
        super(props);
        	this.state = { 
			Name: '',
			email: '',
			message: ''
		};
   
  }
        
    

    static navigationOptions = {
        title: 'تواصل معنا'
    };



    render() {
        return(
        	<View
				style={{
					flex: 1,
					flexDirection: 'column',
			        justifyContent: 'center',
			        padding: 15
			  
				}}>
				<View>
					<TextInput
						style={{
							height: 50
						}}
						value={this.state.Name}
						onChangeText={(value) => this.setState({Name: value})}
						placeholder="أدخل الاسم الكامل"/>
				</View>
				<View>
					<TextInput
						style={{
							height: 50
						}}
						onChangeText={(value) => this.setState({email: value})}
						value={this.state.email}
						placeholder="أدخل الايميل"/>
				</View>
				<View>
					<TextInput
						style={{
							height: 100
						}}
						onChangeText={(value) => this.setState({message: value})}
						value={this.state.message}
						placeholder="أدخل رسالتك هنا"/>
				</View>
				<View
					style={{
						height: 50
					}}>
					<Button
					  	title="أرسل" 
					  	color="#4285f4"
					  	  />
				</View>
			</View>

      
        );

    }
}

export default Contact;

