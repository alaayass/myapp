import React, { Component } from 'react';
import { View, Button, StyleSheet, Text } from 'react-native';
import { Card, Icon, Input } from 'react-native-elements';
import { SecureStore } from 'expo';
import * as firebase from 'firebase';
import Home from './HomeComponent';
import SignUp from './SignUp';





class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            errorMessage: null,
            loading: false,
            user: ""
        }
    }


  componentDidMount() {


    firebase.auth().onAuthStateChanged((user) => {
  if (user != null) {
    console.log("We are authenticated now!");
    this.setState({ user });
  }
  });


   
     

      
  

    
    
  }
    
  

    static navigationOptions = {
        title: 'تسجيل الدخول',
    };



  handleLogin(){
    const {email,password} = this.state;

  firebase.auth().signInWithEmailAndPassword(email, password)
  .then(() => {
  this.props.navigation.navigate('Home');
})
.catch(error => this.setState({ errorMessage: error.message}));

}

    render() {
                const { navigate } = this.props.navigation;
                    const { user } = this.state


        return (
        	
            <View style={styles.container}>
            <Text style={{fontSize: 20}}>أهلاً و سهلاً<Text style={{color:'#e93766', fontSize: 20}}> 
          {user && user.email}!
        </Text></Text>
                  <Text style={{color:'#e93766', fontSize: 30, textAlign: 'right'}}>تسجيل الدخول</Text>
        {this.state.errorMessage &&
          <Text style={{ color: 'red' }}>
            {this.state.errorMessage}
          </Text>}

                <Input
                    placeholder="ايميل المستخدم"
                    leftIcon={{ type: 'font-awesome', name: 'user-o' }}
                    onChangeText={(email) => this.setState({email})}
                    value={this.state.email}
                    containerStyle={styles.formInput}
                    />
                <Input
                    placeholder="كلمة السر"
                    leftIcon={{ type: 'font-awesome', name: 'key' }}
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                    containerStyle={styles.formInput}
                    />
                <View style={styles.formButton}>
                    <Button
                        onPress={this.handleLogin.bind(this)}
                        title="تسجيل الدخول"
                        color="#512DA8"
                        />
                        <Button
                        onPress={() => navigate('SignUp')}
                        title="مستخدم جديد"
                        color="#512DA8"
                        />
                </View>

                
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        margin: 20,
    },
    formInput: {
        margin: 40
    },
    formButton: {
        margin: 60
    }
});

export default Login;