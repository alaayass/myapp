import React, { Component } from 'react';
import { View, Platform, TouchableOpacity, Image, Text, SafeAreaView, ScrollView } from 'react-native';
import OfferDetail from './OfferDetails';
import OffersList from './OffersComponent';
import Contact from './Contactus';
import Login from './login';
import Sadaqa from './Sadaqa';
import Reservation from './ReservationComponent';
import { OFFERS } from '../shared/offers';
import Home from './HomeComponent';
import { createStackNavigator, createDrawerNavigator, DrawerItems, withNavigation } from 'react-navigation';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { fetchOffers, fetchComments } from '../redux/ActionCreators';
import SignUp from './SignUp';
import * as firebase from 'firebase';


const mapStateToProps = state => {
  return {
    offers: state.offers,
    comments: state.comments
  }
}

const mapDispatchToProps = dispatch => ({
  fetchOffers: () => dispatch(fetchOffers()),
  fetchComments: () => dispatch(fetchComments()),
})


const OfferNavigator = createStackNavigator({
        OffersList: { screen: OffersList,
        navigationOptions: ({ navigation }) => ({
              headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() } />          
            }) },
        OfferDetail: { screen: OfferDetail }
    },
    {
        initialRouteName: 'OffersList',
        navigationOptions: ({ navigation }) => ({
            headerStyle: {
                backgroundColor: "#B76016"
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: "#fff"            
            },
            headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() } />
        })
    }
);

 const LoginNavigator = createStackNavigator({
              Login: { screen: Login,
        navigationOptions: ({ navigation }) => ({
              headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() } />          
            }) },
        SignUp: { screen: SignUp }
    },
    {
        initialRouteName: 'Login',
        navigationOptions: ({ navigation }) => ({
            headerStyle: {
                backgroundColor: "#B76016"
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: "#fff"            
            },
            headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() } />
        })
    }
);
 

const ReservationNavigator = createStackNavigator({
    Reservation: { screen: Reservation }
  }, 
  {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#B76016"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
      headerLeft: <Icon name="menu" size={24}
        color= 'white' 
        onPress={ () => navigation.toggleDrawer() } />    
    })
  });
const SadaqaNavigator = createStackNavigator({
    Reservation: { screen: Sadaqa }
  }, 
  {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#B76016"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
      headerLeft: <Icon name="menu" size={24}
        color= 'white' 
        onPress={ () => navigation.toggleDrawer() } />    
    })
  });

const contactNavigator = createStackNavigator({
        Contact: { screen: Contact }
    },
    {
        navigationOptions: ({ navigation }) => ({
            headerStyle: {
                backgroundColor: "#B76016"
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: "#fff"            
            },
            headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() } />
        })
    }
);
const HomeNavigator = createStackNavigator({
    Home: { screen: Home }

  }, {
     


    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#B76016"
      },

      headerTitleStyle: {
          color: "#fff"            
      }, 
       headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() } />
    
  
    }
    )
});
const MainNavigator = createDrawerNavigator({
    Home: 
      { screen: HomeNavigator,
        navigationOptions: {
          title: 'الصفحة الرئيسية',
          drawerLabel: 'الصفحة الرئيسية',
          drawerIcon: ({ tintColor, focused }) => (
            <Icon
              name='home'
              type='font-awesome'            
              size={24}
              color={tintColor}
            />
          )
          

        }
      },
      Login: 
  { screen: LoginNavigator,
    navigationOptions: {
      title: 'تسجيل الدخول',
      drawerLabel: 'تسجيل الدخول',
      drawerIcon: ({ tintColor, focused }) => (
        <Icon
          name='sign-in'
          type='font-awesome'            
          size={24}
          iconStyle={{ color: tintColor }}
        />
      ),
    }
  },

    OffersList: 
      { screen: OfferNavigator,
        navigationOptions: {
          title: 'العروض',
          drawerLabel: 'العروض',
          drawerIcon: ({ tintColor, focused }) => (
            <Icon
              name='list'
              type='font-awesome'            
              size={24}
              color={tintColor}
            />
          )
        }
      },
      Contact: 
      { screen: contactNavigator,
        navigationOptions: {
          title: 'تواصل معنا',
          drawerLabel: 'تواصل معنا',
          drawerIcon: ({ tintColor, focused }) => (
            <Icon
              name='address-card'
              type='font-awesome'            
              size={22}
              color={tintColor}
            />
          )
        }
      },
      Reservation:
      { screen: ReservationNavigator,
        navigationOptions: {
          title: 'احجز الآن',
          drawerLabel: 'احجز الآن',
          drawerIcon: ({ tintColor, focused }) => (
            <Icon
              name='ticket'
              type='font-awesome'            
              size={24}
              color={tintColor}
            />
          ),
        }
      },
      Sadaqa:
      { screen: SadaqaNavigator,
        navigationOptions: {
          title: 'العمرة علينا',
          drawerLabel: 'العمرة علينا',
          drawerIcon: ({ tintColor, focused }) => (
            <Icon
              name='gift'
              type='font-awesome'            
              size={24}
              color={tintColor}
            />
          ),
        }
      },
},
{

  contentComponent:(props)=>(
<SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always', horizontal: 'never' }}>

  <ScrollView>
  <View>
    <Image source={require('./images/user.png')} style={{width: 60, height: 70, marginLeft: 5}} />
         <Text style={{fontSize: 15}}>{props.screenProps}</Text>

    <DrawerItems {...props} />
  </View>
  </ScrollView>
  </SafeAreaView>
)

}
);
class Main extends Component {
  constructor(props) {
        super(props);
        this.state = { 
      user: ""
    };
  }

    componentDidMount() {



    this.props.fetchOffers();
    this.props.fetchComments();


    firebase.auth().onAuthStateChanged((user) => {
  if (user != null) {
    console.log("We are authenticated now!");
    this.setState({ user });
  }
  });


   
     

      
  

    
    
  }

 

  render() {
    const { user } = this.state


 
    return (
      
 

      
         <View style={{flex:1, flexDirection: 'row-reverse', paddingTop: Platform.OS === 'ios' ? 0 : Expo.Constants.statusBarHeight }}>
             
            <MainNavigator screenProps={user.email} />
            
        </View>
        
    );
  }
}
  

export default connect(mapStateToProps, mapDispatchToProps)(Main);