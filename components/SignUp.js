import React, { Component } from 'react';
import { View, Button, StyleSheet } from 'react-native';
import { Card, Icon, Input } from 'react-native-elements';
import * as firebase from 'firebase';
import Home from './HomeComponent';





class SignUp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            error: '',
            loading: false
        }
    }



    
  

    static navigationOptions = {
        title: 'مستخدم جديد',
    };

  handleSignUp(){
    const {email,password} = this.state;

  firebase.auth().createUserWithEmailAndPassword(email, password)
  .then(() => {
  this.props.navigation.navigate('Home');
})
.catch(error => this.setState({ error:'Authentication Failed', loading:false}));

}

    render() {
        return (
            <View style={styles.container}>
                <Input
                    placeholder="ايميل المستخدم"
                    leftIcon={{ type: 'font-awesome', name: 'user-o' }}
                    onChangeText={(email) => this.setState({email})}
                    value={this.state.email}
                    containerStyle={styles.formInput}
                    />
                <Input
                    placeholder="كلمة السر"
                    leftIcon={{ type: 'font-awesome', name: 'key' }}
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                    containerStyle={styles.formInput}
                    />
                <View style={styles.formButton}>
                    <Button
                        onPress={this.handleSignUp.bind(this)}
                        title="إضافة مستخدم"
                        color="#512DA8"
                        />
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        margin: 20,
    },
    formInput: {
        margin: 40,
        paddingLeft: 50
    },
    formButton: {
        margin: 60
    }
});

export default SignUp;