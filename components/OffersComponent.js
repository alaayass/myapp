import React, { Component } from 'react';
import { View, FlatList, Text } from 'react-native';
import { ListItem } from 'react-native-elements';
import { OFFERS } from '../shared/offers';
import OfferDetail from './OfferDetails';
import * as firebase from 'firebase';

import { db } from './config';




class OffersList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            
            offersdata: []
        };
    }

    //define the navigation option and the status bar

    static navigationOptions = {
        title: 'العروض'
    };

      componentWillMount() {
        db.ref('offers').on('value', (data) => {
            console.log(data.toJSON());
            this.setState({ offersdata: data.val() });
        });
    }

    render(){

     //extract the navigator parameter. 
    const { navigate } = this.props.navigation;

    const renderOfferItem = ({item, index}) => {

        return (

                <ListItem
                    key={index}
                    title={<Text style={{textAlign: 'right', fontSize: 20}}>{item.name}</Text>}
                    subtitle={<Text style={{textAlign: 'right'}}>{item.description}</Text>}
                    onPress={() => navigate('OfferDetail', { offerId: item.description,
              offerName: item.name })}

                    hideChevron={true}
                    leftAvatar={{ source: require('./images/x.png')}}
                    bottomDivider
                    
                  />
        );
    };

   

    return (


            <FlatList 
                
                data={Object.values(this.state.offersdata)}
                renderItem={renderOfferItem}
                keyExtractor={item => item.name}  

                />
    );

    }

}


export default OffersList;