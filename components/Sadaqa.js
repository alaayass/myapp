import React, { Component } from 'react';
import { Card, Button, Icon } from 'react-native-elements';
import { View, Text, Image, Style, ImageBackground, TouchableOpacity, StyleSheet  } from 'react-native';

import { MailComposer } from 'expo';



class Sadaqa extends Component {
   sendMail() {
        MailComposer.composeAsync({
            recipients: ['alaa.alyaseen@gmail.com'],
            subject: 'اهدي عمرة',
            body: 'أرجو أن ترسل اسم الشخص الذي تريد إهداء عمرة له و معلومات التواصل. و سنرسل لك معلومات التحويل قريباً'
        })
    }

    static navigationOptions = {
        title: 'العمرة علينا',

       
    };
        



    render() {
    	const { navigate } = this.props.navigation;
        return(

          <View>

          <Card
                 title='اهدي عمرة'
                 image={require('./images/mecca.png')}>
                 <Text style={{marginBottom: 10, textAlign: 'right'}}>أهدي عمرة معنا لشخص تحبه أو لتكسب الأجر</Text><Button
                        title="أرسل طلبك"
                        buttonStyle={{backgroundColor: "#B76016"}}
                        icon={<Icon name='envelope-o' type='font-awesome' color='white' />}
                        onPress={this.sendMail}
                        />

                        </Card>

                        <Card
                 title='أرسل دعوة'
                 image={require('./images/mecca.png')}>
                 <Text style={{marginBottom: 10, textAlign: 'right'}}>أرسل دعوة ليضاف 10 نقاط لرصيدك</Text><Button
                        title="أرسل دعوة"
                        buttonStyle={{backgroundColor: "#B76016"}}
                        icon={<Icon name='envelope-o' type='font-awesome' color='white' />}
                        onPress={this.sendMail}
                        />

                        </Card>

                       
              </View>


         


        	
            
        

           
            
        );

    }
}



export default Sadaqa;