import React, { Component } from 'react';
import { Text, View, ScrollView, FlatList, Button, Share } from 'react-native';
import { Card, Icon } from 'react-native-elements';
import { OFFERS } from '../shared/offers';
import { COMMENTS } from '../shared/comments';
import * as firebase from 'firebase';


import { db } from './config';

    function RenderOfferItem (item){

    
            return(
                <Card
                featuredTitle={item.name}
                image={require('./images/x.png')}>
                <Text style={{margin: 10, textAlign: 'right'}}>{item.description}</Text>
                    <Icon
                    raised
                    reverse
                    type='font-awesome'
                    color='#f50'
                    />
                </Card>

        );
    };
function RenderOffer(props) {

    const offer = props.offer;
    
        if (offer != null) {
            return(
                <Card
                featuredTitle={offer.name}
                image={require('./images/x.png')}>
                    <Text style={{margin: 10, textAlign: 'right'}}>
                        {offer.description}
                    </Text>
                    <Icon
                    raised
                    reverse
                    name={ props.favorite ? 'heart' : 'heart-o'}
                    type='font-awesome'
                    color='#f50'
                    onPress={() => props.favorite ? console.log('Already favorite') : props.onPress()}
                    />
                </Card>
            );
        }
        else {
            return(<View></View>);
        }
}
function RenderComments(props) {

    const comments = props.comments;
            
    const renderCommentItem = ({item, index}) => {
        
        return (
            <View key={index} style={{margin: 10}}>
                <Text style={{fontSize: 14}}>{item.comment}</Text>
                <Text style={{fontSize: 12}}>{item.rating} نجوم</Text>
                <Text style={{fontSize: 12}}>{'-- ' + item.author + ', ' + item.date} </Text>
            </View>
        );
    };
    return (
        <Card title='التعليقات' >
        <FlatList 
            data={comments}
            renderItem={renderCommentItem}
            keyExtractor={item => item.id.toString()}
            />
        </Card>
    );
}



class OfferDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            offers: OFFERS,
            comments: COMMENTS,
            offersdata: [],
            favorites: []
        };
    }

        componentWillMount() {
        db.ref('offers').on('value', (data) => {
            console.log(data.toJSON());
            this.setState({ offersdata: data.val() });
        });
    }


    markFavorite(offerId) {
        this.setState({favorites: this.state.favorites.concat(offerId)});
    }

    static navigationOptions = {
        title: 'تفاصيل العرض'
    };

    render() {      
        const { navigate } = this.props.navigation;


        
        const offerId = this.props.navigation.getParam('offerId','');
        const offerName = this.props.navigation.getParam('offerName','');

              const shareOffer = (title, message, url) => {
                       Share.share({
                                title: title,
                                 message: title + ': ' + message + ' ' + url,
                                 url: url
        },{
          dialogTitle: 'Share ' + title
       })
   }

        return(
           <View>
            <ScrollView>
             <Card
                featuredTitle={offerName}
                image={require('./images/x.png')}>
                <Text style={{margin: 10, textAlign: 'right'}}>{offerId}</Text>
                <View style={{flex: 1, alignItems: "center", flexDirection: 'row'}}>
                    <Icon
                    raised
                    reverse
                    type='font-awesome'
                    name={ this.state.favorite ? 'heart' : 'heart-o'}
                    color='#f50'/><Icon
                    raised
                    reverse
                    name='share'
                    type='font-awesome'
                    color='#51D2A8'
                    onPress={() => shareOffer(offerName, offerId, "http://example/share")} />
                </View>

                </Card>
                <RenderComments comments={this.state.comments.filter((comment) => comment.offerId === offerId)} />
                
            </ScrollView>
            <View>
              <Button
                    onPress={()=> navigate('OffersList')}
                    title="عودة لصفحة العروض"
                    color="#512DA8"
                    accessibilityLabel="Learn more about this purple button"
                    />

            </View>
           
            </View>


            
        );
    }
}

export default OfferDetail;