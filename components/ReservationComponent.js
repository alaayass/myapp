import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, Picker, Switch, Button, Alert } from 'react-native';
import { Card, Input } from 'react-native-elements';
import { db } from './config';
import * as firebase from 'firebase';

import DatePicker from 'react-native-datepicker'

class Reservation extends Component {

    constructor(props) {
        super(props);

        this.state = {
            guests: 1,
            date: '',
            cities: '',
            name: ''
        }
    }

    static navigationOptions = {
        title: 'احجز الآن',
    };
    handleReservation = e => {
    console.log(JSON.stringify(this.state));

     db.ref('/reservation').push({
                    name: this.state.name,
                    noguests: this.state.guests,
                    city: this.state.cities,
                    dateof: this.state.date

  });

    this.setState({
        guests: 1,
            cities: '',
            date: '',
            name: ''
      
    });
    Alert.alert(
  this.state.name,
  'لقد تم الحجز بنجاح راجع مكاتبنا في أقرب وقت ممكن لدفع مبلغ الحجز. الرجاء المراجعة باسم الحجز و إحضار الإقامة أو جواز السفر',
  [
    {
      text: 'Cancel',
      onPress: () => console.log('Cancel Pressed'),
      style: 'cancel',
    },
    {text: 'OK', onPress: () => console.log('OK Pressed')},
  ],
  {cancelable: false},
);

};

  /*  handleReservation() {
        console.log(JSON.stringify(this.state));
        this.setState({
            guests: 1,
            cities: '',
            date: '',
            name: ''
        });
    }*/
    
    render() {
        return(
            <ScrollView>

                     <View>
                <Text style={{margin: 10, textAlign: 'right', fontSize: 18}}>اختر الحملة</Text>
                <Picker
                    style={styles.formItem}
                    selectedValue={this.state.cities}
                    onValueChange={(itemValue, itemIndex) => this.setState({cities: itemValue})}>
                    <Picker.Item label="الرياض-حملة دار الطيبون 160 للشخص 3 أيام" value="الرياض-حملة دار الطيبون 160 للشخص 3 أيام" />
                    <Picker.Item label="القصيم-حملة الرسالة 150 للشخص 3 أيام" value="القصيم-حملة الرسالة 150 للشخص 3 أيام" />
                    <Picker.Item label="جدة-حملة البسام 175 ريال للشخص 3 أيام" value="جدة-حملة البسام 175 ريال للشخص 3 أيام" />
                    <Picker.Item label="الرياض-حملة الأسواف 300 ريال للشخص 5 أيام" value="الرياض-حملة الأسواف 300 ريال للشخص 5 أيام" />
                    <Picker.Item label="الرياض-دار الخير 200 ريال للشخص 3 أيام" value="الرياض-دار الخير 200 ريال للشخص 3 أيام" />
                    
                </Picker>
                </View>

                  <View style={styles.formRow}>
                  <Input
                    placeholder="الحجز باسم السيد/السيدة"
                    leftIcon={{ type: 'font-awesome', name: 'user-o' }}
                    onChangeText={(name) => this.setState({name})}
                    value={this.state.name}
                    
                    />
                    </View>
                <View style={styles.formRow}>
                 

                <Text style={styles.formLabel}>عدد المسافرين</Text>
                <Picker
                    style={styles.formItem}
                    selectedValue={this.state.guests}
                    onValueChange={(itemValue, itemIndex) => this.setState({guests: itemValue})}>
                    <Picker.Item label="1" value="1" />
                    <Picker.Item label="2" value="2" />
                    <Picker.Item label="3" value="3" />
                    <Picker.Item label="4" value="4" />
                    <Picker.Item label="5" value="5" />
                    <Picker.Item label="6" value="6" />
                </Picker>
                </View>
              
             
                <View style={styles.formRow}>
                <Text style={styles.formLabel}>التاريخ و الوقت</Text>
                <DatePicker
                    style={{flex: 2, marginRight: 20}}
                    date={this.state.date}
                    format=''
                    mode="datetime"
                    placeholder="اختر التاريخ و الوقت"
                    minDate="2017-01-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                    dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0
                    },
                    dateInput: {
                        marginLeft: 36
                    }
                    // ... You can check the source to find the other keys. 
                    }}
                    onDateChange={(date) => {this.setState({date: date})}}
                />
                </View>
                <View style={styles.formRow}>
                <Button
                    onPress={this.handleReservation}
                    title="احجز الآن"
                    color="#512DA8"
                    accessibilityLabel="Learn more about this purple button"
                    />
                </View>
            </ScrollView>
        );
    }

};

const styles = StyleSheet.create({
    formRow: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
      flexDirection: 'row',
      margin: 20
    },
    formLabel: {
        fontSize: 18,
        flex: 1
    },
    formItem: {
        flex: 1
    }
});

export default Reservation;