
import * as firebase from 'firebase';

let config = {
  apiKey: "AIzaSyCgR0Ff7BfeXvJlKxmkgzU321v_lbgV2C4",
  authDomain: "omraapp-fd152.firebaseapp.com",
  databaseURL: "https://omraapp-fd152.firebaseio.com",
  projectId: "omraapp-fd152",
  storageBucket: "omraapp-fd152.appspot.com",
  messagingSenderId: "150893156453",
  appId: "1:150893156453:web:b0a49a028c41392e"
 
};

let app = firebase.initializeApp(config);
export const db = app.database();
