import React, { Component } from 'react';
import { View, Text, Image, Style, ImageBackground, TouchableOpacity, Button, StyleSheet  } from 'react-native';
import Contact from './Contactus';
import Reservation from './ReservationComponent';
import * as firebase from 'firebase';




class Home extends Component {

	 constructor(props) {
        super(props);
        this.state = { 
			currentUser: null
		};
        
    }

    static navigationOptions = {
        title: 'الصفحة الرئيسية',

       
    };
     componentDidMount() {
    const { currentUser } = firebase.auth()
    this.setState({ currentUser })
}
        



    render() {
    	const { navigate } = this.props.navigation;
        return(


        	
            
            
            <ImageBackground source={require('./images/h.png')} style={{width: '100%', height: '100%'}}>
            <View>
             <Button
            title="تواصل معنا"
            color="#006400"
            accessibilityLabel="Learn more about this purple button"
            onPress={() => navigate('Contact')}



            />
             <TouchableOpacity onPress={() => navigate('Reservation')}>
      <Image
        style={styles.button}
        source={require('./images/book.png')}
      />
              </TouchableOpacity>

            </View>
            </ImageBackground>


           
            
        );

    }
}

const styles = StyleSheet.create({
  button: {
    width: 200,
    height: 50,
    marginRight:120,
    marginLeft:90,
   marginTop:420,
    paddingTop:10,
    paddingBottom:10
  }
});

export default Home;